package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  int rows;
  int cols;
  List<List<Color>> grid;

  public ColorGrid (int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<>();
    for (int i = 0; i < rows; i++){
      this.grid.add(new ArrayList<Color>());
      for (int j = 0; j < cols; j++) {
        this.grid.get(i).add(null);
      }
    }

  }

  @Override
  public int rows() {
    return rows;
  }

  @Override
  public int cols() {
    return cols;
  }

  @Override
  public List<CellColor> getCells() {
    ArrayList<CellColor> gridcells = new ArrayList<>();
    for (int i = 0; i < grid.size(); i ++) {
      for (int j = 0; j < grid.get(0).size(); j++) {
        CellPosition pos = new CellPosition(i, j);
        CellColor gridCellColor = new CellColor(pos, grid.get(i).get(j));
        gridcells.add(gridCellColor);
      }
    }
    return gridcells;
  }

  @Override
  public Color get(CellPosition pos) {
    Color color = this.grid.get(pos.row()).get(pos.col());
    return color;
  }

  @Override
  public void set(CellPosition pos, Color color) {
    int row = pos.row();
    int col = pos.col();
    this.grid.get(row).set(col, color);
  }


}
