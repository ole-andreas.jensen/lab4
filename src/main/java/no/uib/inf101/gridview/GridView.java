package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {
  private IColorGrid grid;              // Dette er en feltvariabler
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  private static final double INNERMARGIN = 30;

  public GridView(IColorGrid grid) {
    this.setPreferredSize(new Dimension(400, 300));     // Dette er en konstruktøren
    this.grid = grid;
    
  }

  
  @Override 
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
    
  }

  public void drawGrid (Graphics2D g2) {
    double width = this.getWidth() - (2 * OUTERMARGIN);
    double height = this.getHeight() - (2 * OUTERMARGIN);
    Rectangle2D box = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);
    g2.setColor(MARGINCOLOR);
    g2.fill(box);

    CellPositionToPixelConverter CPcov = new CellPositionToPixelConverter (box, grid, INNERMARGIN);

    drawCells(g2, grid, CPcov);

    
  }
  private static void drawCells (Graphics2D g2, CellColorCollection CCC, CellPositionToPixelConverter cptpc) {
    for(CellColor cc: CCC.getCells()) {
      Rectangle2D cell = cptpc.getBoundsForCell(cc.cellPosition());
      if (cc.color() == null) {
        g2.setColor(Color.DARK_GRAY);
      }
      else {
        g2.setColor(cc.color());
      }

      g2.fill(cell);
    } 
  }
}
