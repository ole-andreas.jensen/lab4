package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

import java.awt.geom.Rectangle2D;


public class CellPositionToPixelConverter {
  
// Feltvariabler:
  Rectangle2D box;
  GridDimension gd;
  double margin;

// Konstruktør:
  public CellPositionToPixelConverter (Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;

  }
  
  
  
  public Rectangle2D getBoundsForCell (CellPosition cp) {
    double cellWidth = (box.getWidth() - ((gd.cols()+1) * margin))/gd.cols();
    double cellHeight = (box.getHeight() - ((gd.rows()+1) * margin))/gd.rows();
    double cellX = box.getX() + ((cp.col()+1) * margin) + (cp.col() * cellWidth);
    double cellY = box.getY() + ((cp.row()+1) * margin) + (cp.row() * cellHeight);
    Rectangle2D cell = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    return cell;
  }
}
